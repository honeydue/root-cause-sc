import gitlab
from dotenv import dotenv_values
import os
from anthropic import Anthropic
from mistletoe import markdown
from inscriptis import get_text

config = {
    **dotenv_values(".env"),
    **dotenv_values(".env.secret")
}

if config['GITLAB_TOKEN'] == '':
    gl = gitlab.Gitlab(config['GITLAB_URL'], job_token=os.environ['CI_JOB_TOKEN'])
else:
    gl = gitlab.Gitlab(config['GITLAB_URL'], private_token=config['GITLAB_TOKEN'])

project = gl.projects.get("honeydue/root-cause-sc")
for topic in project.topics:
    print(topic.title())

client = Anthropic(
    api_key=config['ANTHROPIC_SECRET']
)

response = client.messages.create(
    max_tokens=1024,
    messages=[
        {
            "role": "user",
            "content": [{
                "type": "text",
                "text": "Explain this code to me: project = gl.projects.get(\"honeydue/root-cause-sc\") \
                            for topic in project.topics: \
                            print(topic.title())"
            }]
        },
    ],
    model="claude-3-opus-20240229"
)

message_content = response.content[0].text

# Parse Markdown with mistletoe
html_content = markdown(message_content)

text = get_text(html_content)

# Print the parsed HTML content (assuming console supports basic HTML tags)
print(text)

