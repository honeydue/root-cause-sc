# Root Cause Smart Component



## Getting started

- Install Python
- Run `python -m venv "venv"`
- Run `pip install -r requirements.txt`
- Activate the virtual environment
    - On Windows: `venv\Scripts\activate`
    - On macOS/Linux: `source venv/bin/activate`
- Install the project dependencies
    - `pip install -e .`
- Run the application
    - `python script.py`

## To run locally
- create a `.env.secret` file in the root
- add a PAT called `GITLAB_TOKEN`